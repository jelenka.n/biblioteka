import sqlite3

connection = sqlite3.connect("Biblioteka.db")

cursor = connection.cursor()

id_autor = input("\nRedni broj: ")
autor = input("Ime i prezime autora: ")

upit = f"""
INSERT INTO Autor
VALUES ('{id_autor}','{autor}')
"""

cursor.execute(upit)

connection.commit()

connection.close()

print("-------------------------------------------")
print("Autor je uspješno unesen u bazu podataka!")
print("-------------------------------------------")

nazad = input("\nPovratak u osnovni meni: (Da)\n\t\n")

while nazad not in ["Da"]:
    nazad = input("Pogrešan unos! Pokušajte ponovo: ")
if nazad == "Da":
    import main
else:
    pass
