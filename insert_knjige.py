import sqlite3

connection = sqlite3.connect("Biblioteka.db")

cursor = connection.cursor()

isbn = input("\nISBN: ")
naziv = input("Naziv knjige: ")
autor = input("Ime i prezime autora: ")
izdavac = input("Izdavač: ")
grad = input("Mjesto izdavanja: ")
godina = input("Godina izdavanja: ")
datum_unosa = input("Datum unosa knjige u bazu podataka: ")
status = input("Status knjige: ")

upit = f"""
    INSERT INTO Knjige
    VALUES ('{isbn}','{naziv}','{autor}','{izdavac}','{grad}','{godina}','{datum_unosa}','{status}')
    """

cursor.execute(upit)

connection.commit()

connection.close()

print("-------------------------------------------")
print("Knjiga je uspješno unesena u bazu podataka!")
print("-------------------------------------------")

nazad = input("\nPovratak u osnovni meni: (Da)\n\t\n")

while nazad not in ["Da"]:
    nazad = input("Pogrešan unos! Pokušajte ponovo: ")
if nazad == "Da":
    import main
else:
    pass