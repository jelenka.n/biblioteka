import sqlite3

connection = sqlite3.connect("Biblioteka.db")

cursor = connection.cursor()

a = input("-------------------------------------------\nUnesite naziv knjige koju želite izbrisati iz baze: ")

cursor.execute(f'''DELETE FROM Knjige WHERE naziv = '{a}';''')

connection.commit()

connection.close()

print("-------------------------------------------")
print("Knjiga je uspješno izbrisana iz baze podataka!")
print("-------------------------------------------")

nazad = input("\nPovratak u osnovni meni: (Da)\n\t\n")

while nazad not in ["Da"]:
    nazad = input("Pogrešan unos! Pokušajte ponovo: ")
if nazad == "Da":
    import main
else:
    pass
