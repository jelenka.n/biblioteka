import sqlite3

connection = sqlite3.connect("Biblioteka.db")

cursor = connection.cursor()

a = input("--------------------------------------------\nUnesite članski broj korisnika kojeg želite izbrisati iz baze podataka: ")

cursor.execute(f'''DELETE FROM Clanovi WHERE clanski_br = '{a}';''')

connection.commit()

connection.close()

print("-------------------------------------------")
print("Korisnik je uspješno izbrisan iz baze podataka!")
print("-------------------------------------------")

nazad = input("\nPovratak u osnovni meni: (Da)\n\t\n")

while nazad not in ["Da"]:
    nazad = input("Pogrešan unos! Pokušajte ponovo: ")
if nazad == "Da":
    import main
else:
    pass
