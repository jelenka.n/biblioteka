import sqlite3

connection = sqlite3.connect("Biblioteka.db")

cursor = connection.cursor()

cursor.execute("""SELECT * FROM Izdavanje;""")

rezultat = cursor.fetchall()
for x in rezultat:
    print("-------------------------------------------\nISBN: ", x[0])
    print("Naziv: ", x[1])
    print("Autor: ", x[2])
    print("Status: ", x[3])
    print("Članski broj korisnika: ", x[4])
    print("Datum izdavanja knjige: ", x[5])

connection.commit()

connection.close()

nazad = input("\nPovratak u osnovni meni: (Da)\n\t\n")

while nazad not in ["Da"]:
    nazad = input("Pogrešan unos! Pokušajte ponovo: ")
if nazad == "Da":
    import main
else:
    pass