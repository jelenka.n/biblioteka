import sqlite3

connection = sqlite3.connect("Biblioteka.db")

cursor = connection.cursor()

a = input("-------------------------------------------\nUnesite ime i prezime autora, radi provjere da li se nalazi u bazi podataka: ")

lista = []

cursor.execute(f"""SELECT autor FROM Autor WHERE autor == '{a}';""")

rezultat = cursor.fetchall()
for x in rezultat:
    lista.append(x)

if len(lista) == 0:
    print("\nAutor ne postoji u bazi podataka!")
else:
    print("\nAutor postoji u bazi podataka!")

connection.commit()

connection.close()

nazad = input("\nUkoliko želite nastaviti unos autora u bazu podataka unesite Da, za povratak u osnovni meni unesite Povratak: \n\t\n")

while nazad not in ["Da", "Povratak"]:
    nazad = input("Pogrešan unos! Pokušajte ponovo: ")
if nazad == "Da":
    import insert_autori
elif nazad == "Povratak":
    import main
