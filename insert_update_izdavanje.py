import sqlite3

connection = sqlite3.connect("Biblioteka.db")

cursor = connection.cursor()

isbn = input("\nISBN: ")
naziv = input("Naziv knjige: ")
autor = input("Ime i prezime autora: ")
status = input("Status knjige: ")
clanski_br = input("Unesite članski broj korisnika: ")
datum_izd = input("Datum izdavanja knjige: ")

upit = f"""
INSERT INTO Izdavanje
VALUES ('{isbn}','{naziv}','{autor}','{status}','{clanski_br}','{datum_izd}')
"""
cursor.execute(upit)

cursor.execute(f"""UPDATE Izdavanje SET status = "Izdata" WHERE naziv = '{naziv}';""")

cursor.execute(f"""SELECT * FROM Izdavanje WHERE naziv = '{naziv}';""")

rezultat = cursor.fetchall()

for x in rezultat:
    print("-------------------------------------------\nISBN: ", x[0])
    print("Naziv: ", x[1])
    print("Autor: ", x[2])
    print("Status: ", x[3])
    print("Članski broj korisnika: ", x[4])
    print("Datum izdavanja knjige: ", x[5])

connection.commit()

connection.close()

print("-------------------------------------------")
print("Knjiga je uspješno izdata!")
print("-------------------------------------------")

nazad = input("\nPovratak u osnovni meni: (Da)\n\t\n")

while nazad not in ["Da"]:
    nazad = input("Pogrešan unos! Pokušajte ponovo: ")
if nazad == "Da":
    import main
else:
    pass