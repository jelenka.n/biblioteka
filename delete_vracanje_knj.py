import sqlite3

connection = sqlite3.connect("Biblioteka.db")

cursor = connection.cursor()

a = input("-------------------------------------------\nUnesite naziv knjige koju korisnik vraća: ")

cursor.execute(f'''DELETE FROM Izdavanje WHERE naziv = '{a}';''')

connection.commit()

connection.close()

print("-------------------------------------------")
print("Knjiga je uspješno vraćena od strane korisnika!")
print("-------------------------------------------")

nazad = input("\nPovratak u osnovni meni: (Da)\n\t\n")

while nazad not in ["Da"]:
    nazad = input("Pogrešan unos! Pokušajte ponovo: ")
if nazad == "Da":
    import main
else:
    pass
