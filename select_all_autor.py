import sqlite3

connection = sqlite3.connect("Biblioteka.db")

cursor = connection.cursor()

cursor.execute("""SELECT * FROM Autor;""")

rezultat = cursor.fetchall()
for x in rezultat:
    print("-------------------------------------------\nRedni broj: ", x[0])
    print("Naziv: ", x[1])

connection.commit()

connection.close()

nazad = input("\nPovratak u osnovni meni: (Da)\n\t\n")

while nazad not in ["Da"]:
    nazad = input("Pogrešan unos! Pokušajte ponovo: ")
if nazad == "Da":
    import main
else:
    pass