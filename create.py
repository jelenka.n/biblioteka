import sqlite3

connection = sqlite3.connect("Biblioteka.db")

cursor = connection.cursor()

create_table_knjige = """CREATE TABLE Knjige (
isbn text,
naziv text,
autor text,
izdavac text,
grad text,
godina text,
datum_unosa text,
status text);"""

create_table_izd = """CREATE TABLE Izdavanje (
isbn text,
naziv text,
autor text,
status text,
clanski_br int,
datum_izd text);"""

create_table_autor = """CREATE TABLE Autor (
id_autor int,
autor text);"""

create_table_korisnici = """CREATE TABLE Korisnici (
clanski_br int,
ime text,
prezime text,
adresa text,
telefon text);"""

cursor.execute(create_table_knjige)
cursor.execute(create_table_izd)
cursor.execute(create_table_autor)
cursor.execute(create_table_korisnici)

connection.close()

print("\nTabele uspješno kreirane!")