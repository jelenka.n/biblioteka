import sqlite3

connection = sqlite3.connect("Biblioteka.db")

cursor = connection.cursor()

cursor.execute("""SELECT * FROM Knjige;""")

rezultat = cursor.fetchall()
for x in rezultat:
    print("-------------------------------------------\nISBN: ", x[0])
    print("Naziv: ", x[1])
    print("Autor: ", x[2])
    print("Izdavač: ", x[3] + ",", x[4]  + ",", x[5])
    print("Datum unošenja u bazu: ", x[6])
    print("Status: ", x[7])

connection.commit()

connection.close()

nazad = input("\nPovratak u osnovni meni: (Da)\n\t\n")

while nazad not in ["Da"]:
    nazad = input("Pogrešan unos! Pokušajte ponovo: ")
if nazad == "Da":
    import main
else:
    pass