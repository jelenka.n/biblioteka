import sqlite3

connection = sqlite3.connect("Biblioteka.db")

cursor = connection.cursor()

cursor.execute("""SELECT * FROM Korisnici;""")

rezultat = cursor.fetchall()
for x in rezultat:
    print("-------------------------------------------\nČlanski broj: ", x[0])
    print("Ime: ", x[1])
    print("Prezime: ", x[2])
    print("Adresa: ", x[3])
    print("Telefon: ", x[4])

connection.commit()

connection.close()

nazad = input("\nPovratak u osnovni meni: (Da)\n\t\n")

while nazad not in ["Da"]:
    nazad = input("Pogrešan unos! Pokušajte ponovo: ")
if nazad == "Da":
    import main
else:
    pass