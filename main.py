pitanje = input("\nBaza podataka - Biblioteka\n\nIzaberite opciju:\n\t1. Unos podataka u bazu,\n\t2. Prikaz podataka u bazi,\n\t3. Izdavanje i vraćanje knjiga,\n\t4. Brisanje podataka iz baze\n\t\n")

while pitanje not in ["1", "2", "3", "4", "5"]:
    pitanje = input("Pogrešan unos! Pokušajte ponovo: ")
if pitanje == "1":
    pitanje1 = input("\nIzaberite opciju:\n\t1. Unos novih knjiga u bazu,\n\t2. Unos novih članova u bazu,\n\t3. Unos autora u bazu\n\t\n")
    if pitanje1 == "1":
        print("\nUnos nove knjige u bazu\n-------------------------------------------")
        import insert_knjige
    elif pitanje1 == "2":
        print("\nUnos novog korisnika biblioteke u bazu\n-------------------------------------------")
        import insert_korisnici
    elif pitanje1 == "3":
        print("\nUnos novog autora u bazu\n-------------------------------------------")
        import select_odr_autora
elif pitanje == "2":
    pitanje2 = input("\nIzaberite opciju:\n\t1. Pregled knjiga u bazi,\n\t2. Pregled korisnika biblioteke u bazi,\n\t3. Pregled izdatih knjiga,\n\t4. Pregled autora u bazi\n\t\n")
    if pitanje2 == "1":
        print("\nKnjige unesene u bazu podataka\n-------------------------------------------")
        import select_all_knjige
    elif pitanje2 == "2":
        print("\nČlanovi biblioteke\n-------------------------------------------")
        import select_all_korisnici
    elif pitanje2 == "3":
        print("\nIzdate knjige\n-------------------------------------------")
        import select_all_izd
    elif pitanje2 == "4":
        print("\nIzdate knjige\n-------------------------------------------")
        import select_all_autor
elif pitanje == "3":
    print("\nIzdavanje i vraćanje knjiga\n-------------------------------------------")
    pitanje3 = input("\nIzaberite opciju:\n\t1. Izdavanje knjiga,\n\t2. Vraćanje knjiga\n\t\n")
    if pitanje3 == "1":
        print("\nIzdavanje knjiga\n-------------------------------------------")
        import insert_update_izdavanje
    elif pitanje3 == "2":
        print("\nVraćanje knjiga\n-------------------------------------------")
        import delete_vracanje_knj
elif pitanje == "4":
    pitanje4 = input("\nIzaberite opciju:\n\t1. Brisanje knjiga iz baze podataka,\n\t2. Brisanje korisnika iz baze podataka\n\t\n")
    if pitanje4 == "1":
        print("\nBrisanje knjiga iz baze podataka\n-------------------------------------------")
        import delete_knjige
    elif pitanje4 == "2":
        print("\nBrisanje korisnika iz baze podataka\n-------------------------------------------")
        import delete_korisnik

